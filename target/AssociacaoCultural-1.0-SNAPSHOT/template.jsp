<%-- 
    Document   : layout
    Created on : 29 de abr. de 2021, 21:31:11
    Author     : leola
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="icon" type="image/png" href="images/favicon.ico"  />
        <!--===============================================================================================-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/css/animsition.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Associação Cultural</title>
    </head>
    <body>
         <div class="tudo">
            <header>
                <nav id="header"class="navbar navbar-dark bg-dark">
                    <span class="navbar-brand mb-0 h1">Associação Cultural</span>
                </nav>
            </header>
       
            <div class="menuBar">
                <input type="checkbox" id="check">
                <label id="icone" for="check"><img src="img/icone.png"></label>

            <div class="barra">
                <nav class="menu">
                    <a href="cs?ac=home"><div class="link">Home</div></a>
                    <a href="cs?ac=listaEventos"><div class="link">Lista de eventos</div></a>
                    <a href="cs?ac=cadastroEventos"><div class="link">Cadastrar eventos</div></a>
                    <a href="cs?ac=cadastroEventos"><div class="link">Comprar Ingresso</div></a>
                    <a href="cs?ac=cadastroEventos"><div class="link">Relatório</div></a>
                    <a href="cs?ac=logout"><div class="link">Sair</div></a>
                </nav>
            </div>
               </div>
            
            <div class="conteudo">
               <c:if test="${requestScope.page==null}">
                <jsp:include page="home.jsp"/>
            </c:if>
              <c:if test="${requestScope.page!=null}">
                <jsp:include page="${requestScope.page}"/>
            </c:if>
            </div>
               
         </div>
           
      
            
            
            
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <!--===============================================================================================-->
        <script src="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/js/animsition.min.js"></script>
        <!--===============================================================================================-->
    </body>
</html>
