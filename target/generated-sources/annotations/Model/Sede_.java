package Model;

import Model.Evento;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-05-13T01:02:02")
@StaticMetamodel(Sede.class)
public class Sede_ { 

    public static volatile SingularAttribute<Sede, Integer> setor5;
    public static volatile SingularAttribute<Sede, Integer> setor6;
    public static volatile SingularAttribute<Sede, Integer> cadeirante;
    public static volatile SingularAttribute<Sede, Integer> capacidade;
    public static volatile SingularAttribute<Sede, Integer> cabine;
    public static volatile ListAttribute<Sede, Evento> eventoList;
    public static volatile SingularAttribute<Sede, String> nome;
    public static volatile SingularAttribute<Sede, Integer> setor3;
    public static volatile SingularAttribute<Sede, Integer> idsede;
    public static volatile SingularAttribute<Sede, Integer> setor4;
    public static volatile SingularAttribute<Sede, Integer> setor1;
    public static volatile SingularAttribute<Sede, Integer> setor2;

}