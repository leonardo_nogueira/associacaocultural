package Model;

import Model.Evento;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-05-13T01:02:02")
@StaticMetamodel(Empreendimento.class)
public class Empreendimento_ { 

    public static volatile SingularAttribute<Empreendimento, Evento> evento;
    public static volatile SingularAttribute<Empreendimento, Integer> idempreendimento;
    public static volatile SingularAttribute<Empreendimento, Double> despesas;
    public static volatile SingularAttribute<Empreendimento, Double> lucro;

}