package Model;

import Model.Empreendimento;
import Model.Ingresso;
import Model.Sede;
import Model.Usuario;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-05-13T01:02:02")
@StaticMetamodel(Evento.class)
public class Evento_ { 

    public static volatile ListAttribute<Evento, Ingresso> ingressoList;
    public static volatile SingularAttribute<Evento, Integer> idevento;
    public static volatile SingularAttribute<Evento, Sede> sede;
    public static volatile SingularAttribute<Evento, Ingresso> ingresso;
    public static volatile ListAttribute<Evento, Empreendimento> empreendimentoList;
    public static volatile SingularAttribute<Evento, String> nome;
    public static volatile SingularAttribute<Evento, Usuario> usuario;

}