package Model;

import Model.Evento;
import Model.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-05-13T01:02:02")
@StaticMetamodel(Ingresso.class)
public class Ingresso_ { 

    public static volatile SingularAttribute<Ingresso, Double> valorIngresso;
    public static volatile SingularAttribute<Ingresso, Date> dataEvento;
    public static volatile SingularAttribute<Ingresso, String> setor;
    public static volatile SingularAttribute<Ingresso, Evento> evento;
    public static volatile SingularAttribute<Ingresso, String> doacao;
    public static volatile SingularAttribute<Ingresso, String> inteira;
    public static volatile ListAttribute<Ingresso, Evento> eventoList;
    public static volatile SingularAttribute<Ingresso, Integer> idingresso;
    public static volatile SingularAttribute<Ingresso, Usuario> usuario;
    public static volatile SingularAttribute<Ingresso, String> meiaEntrada;

}