package Model;

import Model.Evento;
import Model.Ingresso;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2021-05-13T01:02:02")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile ListAttribute<Usuario, Ingresso> ingressoList;
    public static volatile SingularAttribute<Usuario, String> senha;
    public static volatile ListAttribute<Usuario, Evento> eventoList;
    public static volatile SingularAttribute<Usuario, String> admin;
    public static volatile SingularAttribute<Usuario, String> nome;
    public static volatile SingularAttribute<Usuario, String> login;
    public static volatile SingularAttribute<Usuario, Integer> idusuario;

}