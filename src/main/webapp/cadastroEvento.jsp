<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="icon" type="image/png" href="images/favicon.ico"  />
        <!--===============================================================================================-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/css/animsition.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css.css" />
        <!--===============================================================================================-->
        <title>Associação Cultural</title>
    </head>
    <body>
        
      
        <br><br><br><br>
     <div class="d-flex justify-content-center">
        <div class="card" style="width: 18rem;">
             <%
                 Usuario u=(Usuario)(request.getAttribute("user")==null?new Usuario():request.getAttribute("user"));
                if(request.getAttribute("msgError") != null){%>
            <div class="alert alert-danger">
                
                <%=
                    request.getAttribute("msgError")
                %>
                
            </div>
             <%}%>
               <div class="card-body">
                   <center>
                    <h5 class="card-title">Cadastro de Eventos</h5>
                   </center>
                    <form method="POST" Action="cs">
                        <input type="hidden" name="ac" value="saveEvento"/>
                        <div class="form-group">
                             <label for="nameEvento">Nome do Evento</label>
                            <input id="nome" type="text" class="form-control" name="nome" value="" required autofocus="" placeholder="Nome" required >
                            
                        </div>
                        <div class="form-group">
                            
                            <label for="dataEvento" class="form-label">Data de realização</label>
        <input required="" type="date" class="form-control" id="dataEvento" name="dataEvento"
             value=""  >
                            
                        </div>
                        <div class="form-group">
                            <label for="descricao">Hora da realização</label>
                            <input type="password" name="descricao" class="form-control" id="exampleInputPassword1" required placeholder="hora da realização">
                        </div>
                       
                        <center>
                            <button type="submit" class="btn btn-primary">Cadastrar</button><br><br>
                    
                        </center>
                   
                  </form>
            </div>
        </div>
     </div> 
        
        
        
        
        
        
        
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <!--===============================================================================================-->
        <script src="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/js/animsition.min.js"></script>
        <!--===============================================================================================-->
    </body>
</html>
