
<%@page import="Model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="icon" type="image/png" href="images/favicon.ico"  />
        <!--===============================================================================================-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/css/animsition.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css.css" />
        <!--===============================================================================================-->
        <title>Associação Cultural</title>
    </head>
    <body>
         <header>
                <nav id="header"class="navbar navbar-dark bg-dark">
                    <span class="navbar-brand mb-0 h1">Seja bem vindo a Associação Cultural</span>
                </nav>
            </header>
    
        <br><br><br><br>
     <div class="d-flex justify-content-center">
        <div class="card" style="width: 18rem;">
             <%
                 Usuario u=(Usuario)(request.getAttribute("user")==null?new Usuario():request.getAttribute("user"));
                if(request.getAttribute("msgError") != null){%>
            <div class="alert alert-danger">
                
                <%=
                    request.getAttribute("msgError")
                %>
                
            </div>
             <%}%>
               <div class="card-body">
                   <center>
                    <h5 class="card-title">Cadastro</h5>
                   </center>
                    <form method="POST" Action="cs">
                        <input type="hidden" name="ac" value="saveUser"/>
                        <div class="form-group">
                             <label for="name">Nome</label>
                            <input id="nome" type="text" class="form-control" name="name" value="<%=u.getNome()%>" required autofocus="" placeholder="Nome">
                            
                        </div>
                        <div class="form-group">
                            
                            <label for="exampleInputEmail1">Login</label>
                            <input id="login" type="text" class="form-control" name="login" value="" required autofocus="" placeholder="Login">
                            
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                       
                        <center>
                            <button type="submit" class="btn btn-primary">Cadastrar</button><br><br>
                    <a href="cs">Cancelar</button>
                        </center>
                   
                  </form>
            </div>
        </div>
     </div> 
        
        
        
        
        
        
        
        
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <!--===============================================================================================-->
        <script src="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/js/animsition.min.js"></script>
        <!--===============================================================================================-->
    </body>
</html>
