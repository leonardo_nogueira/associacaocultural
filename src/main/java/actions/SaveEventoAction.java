/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions;

import GenericCommander.GenericCommander;
import Model.Evento;
import Model.Usuario;
import Model.dao.EventoDao;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leola
 */
public class SaveEventoAction extends GenericCommander{

    public SaveEventoAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            EventoDao.getCon().getTransaction().begin();
            Evento evento= new Evento (0,
                    request.getParameter("nome"),
                    sdf.parse(request.getParameter("dataEvento")),
                    request.getParameter("descricao"));
            
            evento.setUsuario((Usuario) request.getSession().getAttribute("user"));
            
            EventoDao.getCon().persist(evento);
            
            EventoDao.getCon().getTransaction().commit();

            request.setAttribute("msg", "Evento");
            
        

            new ViewHomeLogadoAction(true).executa(request, response);
            
        } catch (ParseException ex) {
            throw new ServletException("Erro ao converter a data");
        }
    }
    
}
