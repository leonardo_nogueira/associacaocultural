/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions;


import GenericCommander.GenericCommander;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leola
 */
public class CadastroAction extends GenericCommander{

    public CadastroAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
           // response.sendRedirect("cadastro.jsp");
        RequestDispatcher rd = request.getRequestDispatcher("cadastro.jsp");
        
        rd.forward(request, response);
        
    }
    
}
