/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions;

import GenericCommander.GenericCommander;
import Model.Usuario;
import Model.dao.EventoDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leola
 */
public class ViewListaEventosAction extends GenericCommander{

    public ViewListaEventosAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("template.jsp");
        
        request.setAttribute("page", "listaEventos.jsp");
        
        rd.forward(request, response);
        request.setAttribute("evento", EventoDao.buscarListaUsuario((Usuario)request.getSession().getAttribute("user")));
    }

}