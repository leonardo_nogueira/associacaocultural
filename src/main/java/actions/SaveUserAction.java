/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actions;

import GenericCommander.GenericCommander;
import Model.Usuario;
import Model.dao.UsuarioDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leola
 */
public class SaveUserAction extends GenericCommander{

    public SaveUserAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        Usuario user= new Usuario (0,
        request.getParameter("name"),
        request.getParameter("login"),
        request.getParameter("password"));
        
        
        if(UsuarioDao.existeLogin(user)){
            request.setAttribute("msgError", "Login ja existente no sistema!!");
            user.setLogin("");
            request.setAttribute("user", user);
            new CadastroAction(false).executa(request,response);
        }else{
            
        UsuarioDao.getCon().getTransaction().begin();
        UsuarioDao.getCon().persist(user);
        UsuarioDao.getCon().getTransaction().commit();
        request.setAttribute("msg", "Usuario criado com sucesso!!");
        
        new ViewLoginAction(false).executa(request,response);
        }
    }
    
}
