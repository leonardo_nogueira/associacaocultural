/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Principal
 */
@Entity
@Table(catalog = "associacaocultural", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"idsede"})})
@NamedQueries({
    @NamedQuery(name = "Sede.findAll", query = "SELECT s FROM Sede s"),
    @NamedQuery(name = "Sede.findByIdsede", query = "SELECT s FROM Sede s WHERE s.idsede = :idsede"),
    @NamedQuery(name = "Sede.findByNome", query = "SELECT s FROM Sede s WHERE s.nome = :nome"),
    @NamedQuery(name = "Sede.findBySetor1", query = "SELECT s FROM Sede s WHERE s.setor1 = :setor1"),
    @NamedQuery(name = "Sede.findBySetor2", query = "SELECT s FROM Sede s WHERE s.setor2 = :setor2"),
    @NamedQuery(name = "Sede.findBySetor3", query = "SELECT s FROM Sede s WHERE s.setor3 = :setor3"),
    @NamedQuery(name = "Sede.findBySetor4", query = "SELECT s FROM Sede s WHERE s.setor4 = :setor4"),
    @NamedQuery(name = "Sede.findBySetor5", query = "SELECT s FROM Sede s WHERE s.setor5 = :setor5"),
    @NamedQuery(name = "Sede.findBySetor6", query = "SELECT s FROM Sede s WHERE s.setor6 = :setor6"),
    @NamedQuery(name = "Sede.findByCapacidade", query = "SELECT s FROM Sede s WHERE s.capacidade = :capacidade"),
    @NamedQuery(name = "Sede.findByCabine", query = "SELECT s FROM Sede s WHERE s.cabine = :cabine"),
    @NamedQuery(name = "Sede.findByCadeirante", query = "SELECT s FROM Sede s WHERE s.cadeirante = :cadeirante")})
public class Sede implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idsede;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor1;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor2;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor3;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor4;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor5;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int setor6;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int capacidade;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int cabine;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int cadeirante;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sede")
    private List<Evento> eventoList;

    public Sede() {
    }

    public Sede(Integer idsede) {
        this.idsede = idsede;
    }

    public Sede(Integer idsede, String nome, int setor1, int setor2, int setor3, int setor4, int setor5, int setor6, int capacidade, int cabine, int cadeirante) {
        this.idsede = idsede;
        this.nome = nome;
        this.setor1 = setor1;
        this.setor2 = setor2;
        this.setor3 = setor3;
        this.setor4 = setor4;
        this.setor5 = setor5;
        this.setor6 = setor6;
        this.capacidade = capacidade;
        this.cabine = cabine;
        this.cadeirante = cadeirante;
    }

    public Integer getIdsede() {
        return idsede;
    }

    public void setIdsede(Integer idsede) {
        this.idsede = idsede;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSetor1() {
        return setor1;
    }

    public void setSetor1(int setor1) {
        this.setor1 = setor1;
    }

    public int getSetor2() {
        return setor2;
    }

    public void setSetor2(int setor2) {
        this.setor2 = setor2;
    }

    public int getSetor3() {
        return setor3;
    }

    public void setSetor3(int setor3) {
        this.setor3 = setor3;
    }

    public int getSetor4() {
        return setor4;
    }

    public void setSetor4(int setor4) {
        this.setor4 = setor4;
    }

    public int getSetor5() {
        return setor5;
    }

    public void setSetor5(int setor5) {
        this.setor5 = setor5;
    }

    public int getSetor6() {
        return setor6;
    }

    public void setSetor6(int setor6) {
        this.setor6 = setor6;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getCabine() {
        return cabine;
    }

    public void setCabine(int cabine) {
        this.cabine = cabine;
    }

    public int getCadeirante() {
        return cadeirante;
    }

    public void setCadeirante(int cadeirante) {
        this.cadeirante = cadeirante;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsede != null ? idsede.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sede)) {
            return false;
        }
        Sede other = (Sede) object;
        if ((this.idsede == null && other.idsede != null) || (this.idsede != null && !this.idsede.equals(other.idsede))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Sede[ idsede=" + idsede + " ]";
    }
    
}
