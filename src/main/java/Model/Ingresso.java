/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Principal
 */
@Entity
@Table(catalog = "associacaocultural", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"idingresso"})})
@NamedQueries({
    @NamedQuery(name = "Ingresso.findAll", query = "SELECT i FROM Ingresso i"),
    @NamedQuery(name = "Ingresso.findByIdingresso", query = "SELECT i FROM Ingresso i WHERE i.idingresso = :idingresso"),
    @NamedQuery(name = "Ingresso.findByValorIngresso", query = "SELECT i FROM Ingresso i WHERE i.valorIngresso = :valorIngresso"),
    @NamedQuery(name = "Ingresso.findByMeiaEntrada", query = "SELECT i FROM Ingresso i WHERE i.meiaEntrada = :meiaEntrada"),
    @NamedQuery(name = "Ingresso.findByDoacao", query = "SELECT i FROM Ingresso i WHERE i.doacao = :doacao"),
    @NamedQuery(name = "Ingresso.findByInteira", query = "SELECT i FROM Ingresso i WHERE i.inteira = :inteira"),
    @NamedQuery(name = "Ingresso.findBySetor", query = "SELECT i FROM Ingresso i WHERE i.setor = :setor"),
    @NamedQuery(name = "Ingresso.findByDataEvento", query = "SELECT i FROM Ingresso i WHERE i.dataEvento = :dataEvento")})
public class Ingresso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idingresso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_ingresso", nullable = false)
    private double valorIngresso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "meia_Entrada", nullable = false, length = 3)
    private String meiaEntrada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(nullable = false, length = 3)
    private String doacao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(nullable = false, length = 3)
    private String inteira;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String setor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_evento", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataEvento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingresso")
    private List<Evento> eventoList;
    @JoinColumn(name = "evento", referencedColumnName = "idevento", nullable = false)
    @ManyToOne(optional = false)
    private Evento evento;
    @JoinColumn(name = "usuario", referencedColumnName = "idusuario", nullable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public Ingresso() {
    }

    public Ingresso(Integer idingresso) {
        this.idingresso = idingresso;
    }

    public Ingresso(Integer idingresso, double valorIngresso, String meiaEntrada, String doacao, String inteira, String setor, Date dataEvento) {
        this.idingresso = idingresso;
        this.valorIngresso = valorIngresso;
        this.meiaEntrada = meiaEntrada;
        this.doacao = doacao;
        this.inteira = inteira;
        this.setor = setor;
        this.dataEvento = dataEvento;
    }

    public Integer getIdingresso() {
        return idingresso;
    }

    public void setIdingresso(Integer idingresso) {
        this.idingresso = idingresso;
    }

    public double getValorIngresso() {
        return valorIngresso;
    }

    public void setValorIngresso(double valorIngresso) {
        this.valorIngresso = valorIngresso;
    }

    public String getMeiaEntrada() {
        return meiaEntrada;
    }

    public void setMeiaEntrada(String meiaEntrada) {
        this.meiaEntrada = meiaEntrada;
    }

    public String getDoacao() {
        return doacao;
    }

    public void setDoacao(String doacao) {
        this.doacao = doacao;
    }

    public String getInteira() {
        return inteira;
    }

    public void setInteira(String inteira) {
        this.inteira = inteira;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public Date getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(Date dataEvento) {
        this.dataEvento = dataEvento;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idingresso != null ? idingresso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingresso)) {
            return false;
        }
        Ingresso other = (Ingresso) object;
        if ((this.idingresso == null && other.idingresso != null) || (this.idingresso != null && !this.idingresso.equals(other.idingresso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Ingresso[ idingresso=" + idingresso + " ]";
    }
    
}
