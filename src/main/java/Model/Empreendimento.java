/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Principal
 */
@Entity
@Table(catalog = "associacaocultural", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"idempreendimento"})})
@NamedQueries({
    @NamedQuery(name = "Empreendimento.findAll", query = "SELECT e FROM Empreendimento e"),
    @NamedQuery(name = "Empreendimento.findByIdempreendimento", query = "SELECT e FROM Empreendimento e WHERE e.idempreendimento = :idempreendimento"),
    @NamedQuery(name = "Empreendimento.findByLucro", query = "SELECT e FROM Empreendimento e WHERE e.lucro = :lucro"),
    @NamedQuery(name = "Empreendimento.findByDespesas", query = "SELECT e FROM Empreendimento e WHERE e.despesas = :despesas")})
public class Empreendimento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idempreendimento;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double lucro;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private double despesas;
    @JoinColumn(name = "evento", referencedColumnName = "idevento", nullable = false)
    @ManyToOne(optional = false)
    private Evento evento;

    public Empreendimento() {
    }

    public Empreendimento(Integer idempreendimento) {
        this.idempreendimento = idempreendimento;
    }

    public Empreendimento(Integer idempreendimento, double lucro, double despesas) {
        this.idempreendimento = idempreendimento;
        this.lucro = lucro;
        this.despesas = despesas;
    }

    public Integer getIdempreendimento() {
        return idempreendimento;
    }

    public void setIdempreendimento(Integer idempreendimento) {
        this.idempreendimento = idempreendimento;
    }

    public double getLucro() {
        return lucro;
    }

    public void setLucro(double lucro) {
        this.lucro = lucro;
    }

    public double getDespesas() {
        return despesas;
    }

    public void setDespesas(double despesas) {
        this.despesas = despesas;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempreendimento != null ? idempreendimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empreendimento)) {
            return false;
        }
        Empreendimento other = (Empreendimento) object;
        if ((this.idempreendimento == null && other.idempreendimento != null) || (this.idempreendimento != null && !this.idempreendimento.equals(other.idempreendimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Empreendimento[ idempreendimento=" + idempreendimento + " ]";
    }
    
}
