/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.dao;

import Dao.DaoBase;
import Model.Usuario;
import java.sql.SQLException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
//import sun.jvm.hotspot.utilities.soql.SOQLException;

/**
 *
 * @author leola
 */
public class UsuarioDao extends DaoBase{

    public static boolean existeLogin(Usuario user) {
        Query q = getCon().createQuery("select count(u) from Usuario u where u.login = :log ");
        
        q.setParameter("log", user.getLogin());
        
         return (long)q.getSingleResult() >= 1; 
        
    }
    public Usuario verifyUser(String login, String password) throws SQLException{
        Query q= getCon().createQuery("SELECT u FROM Usuario u Where u.login= :login AND u.senha= :senha");
        q.setParameter("login", login);
        q.setParameter("senha", password);
      //  Query q= getCon().createQuery("SELECT u FROM Usuario u Where u.login= :login AND u.passsword= :password");
     //   q.setParameter("login", login);
    //    q.setParameter("password", password);
        
        
        try{
        return (Usuario) q.getSingleResult();
        }catch(NonUniqueResultException e){
            throw new SQLException("Foi encontrado mais de duas usuarios no banco, contate o administrador");
        }catch(NoResultException e){
            return null;
        }
    } 
}
