/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.dao;

import Dao.DaoBase;
import Model.Evento;
import Model.Usuario;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
//import sun.jvm.hotspot.utilities.soql.SOQLException;

/**
 *
 * @author leola
 */
public class EventoDao extends DaoBase{

    public static List<Evento>buscarListaUsuario(Usuario user) {
        Query q = getCon().createQuery("SELECT e FROM Evento e WHERE e.usuario.idusuario= :user  ");
        
        q.setParameter("user", user.getIdusuario());
        
         return q.getResultList(); 
        
    }
   
} 

