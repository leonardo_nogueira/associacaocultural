/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CentralServlet;

import actions.ViewListaEventosAction;
import GenericCommander.GenericCommander;
import actions.ViewLogoutAction;
import actions.CadastroAction;
import actions.SaveEventoAction;
import actions.LoginVerifyAction;
import actions.SaveUserAction;
import actions.ViewCadastroEvento;
import actions.ViewHomeLogadoAction;
import actions.ViewLoginAction;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Request;


@WebServlet(name = "CentralServlet", urlPatterns = {"/cs"})
public class CentralServlet extends HttpServlet {
    
    static HashMap<String,GenericCommander> comandos;
    static{
        comandos = new HashMap<>();
        
        comandos.put(null, new ViewLoginAction(false));
        comandos.put("log", new LoginVerifyAction(false));
        comandos.put("cad", new CadastroAction(false));
        comandos.put("saveUser", new SaveUserAction(false));
        comandos.put("home", new ViewHomeLogadoAction(true));
        comandos.put("logout", new ViewLogoutAction(true));
        comandos.put("listaEventos", new ViewListaEventosAction(true));
        comandos.put("cadastroEventos", new ViewCadastroEvento(true));
        comandos.put("saveEvento", new SaveEventoAction(true));
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String botao = request.getParameter("btn1");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String acao= request.getParameter("ac");
            
            try{
                if(!comandos.get(acao).isIsLogado()||request.getSession().getAttribute("user") !=null ){
                    
            comandos.get(acao).executa(request, response);
            
                }else{
                    request.setAttribute ("msg","Acesso não autorizado!");
                    new ViewLoginAction(false).executa(request, response);
                }
            }catch(Exception e){
                RequestDispatcher rd= request.getRequestDispatcher("error.jsp");
                request.setAttribute("Error", e.getMessage()==null?"ação não encontrada":e.getMessage());
                rd.forward(request, response);
            }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
